### How do I get set up? ###

* Create a new MySQL database
* Open ***DatabaseCredentials.java*** and configure it according to your database settings
* Run ***SetupDatabase.java*** to populate the database with tables and dummy-data
* Run ***mainClass.java*** to initiate the program

### Program usage ###

The program accepts input through the console, the following commands and parameters are available

* **ADD** [ *airport* | *airline* | *flight* ]
* **LIST** [ *airport* | *airline* | *flight* | *ticket* ]
* **FIND** [ *flight* | *ticket* ]
* **BOOK** [ *flight* ]

To see the commands again, enter ***HELP***.

To cancel a command, enter ***CANCEL*** any time during input.