-- Tables
CREATE TABLE IF NOT EXISTS `airlines` (`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(150) NOT NULL, `flights` varchar(150) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10
CREATE TABLE IF NOT EXISTS `airports` (`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(150) NOT NULL, `code` varchar(150) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6
CREATE TABLE IF NOT EXISTS `flights` (`id` int(11) NOT NULL AUTO_INCREMENT, `flightNumber` int(11) NOT NULL, `airlineName` varchar(150) NOT NULL, `deptDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `deptAirport` varchar(3) NOT NULL, `arrAirport` varchar(3) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5
CREATE TABLE IF NOT EXISTS `tickets` (`id` int(11) NOT NULL AUTO_INCREMENT, `fname` varchar(70) NOT NULL, `lname` varchar(70) NOT NULL, `flight` int(11) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5
-- Airline data
INSERT INTO airlines (name) VALUES ('Scandinavian Airlines')
INSERT INTO airlines (name) VALUES ('Gotlandsflyg')
INSERT INTO airlines (name) VALUES ('City Airline AB')
INSERT INTO airlines (name) VALUES ('Nordic Regional')
-- Airport data
INSERT INTO airports (name, code) VALUES ('Stockholm-Arlanda flygplats', 'ARN')
INSERT INTO airports (name, code) VALUES ('Stockholm-Bromma flygplats', 'BMA')
INSERT INTO airports (name, code) VALUES ('Visby Airport', 'VBY')
INSERT INTO airports (name, code) VALUES ('Kiruna Airport', 'KRN')
-- Flight data
INSERT INTO flights (flightNumber, airlineName, deptDate, deptAirport, arrAirport) VALUES (717, 'Gotlandsflyg', '2015-01-01 08:00:00', 'ARN', 'BMA')
INSERT INTO flights (flightNumber, airlineName, deptDate, deptAirport, arrAirport) VALUES (414, 'City Airline AB', '2015-01-02 08:00:00', 'BMA', 'ARN')
INSERT INTO flights (flightNumber, airlineName, deptDate, deptAirport, arrAirport) VALUES (116, 'Scandinavian Airlines', '2015-01-03 08:00:00', 'ARN', 'VBY')
INSERT INTO flights (flightNumber, airlineName, deptDate, deptAirport, arrAirport) VALUES (311, 'Nordic Regional', '2015-01-04 08:00:00', 'VBY', 'ARN')
-- Add flight data to airlines
UPDATE airlines SET flights = (SELECT id FROM flights WHERE airlineName = 'Gotlandsflyg') WHERE name = 'Gotlandsflyg'
UPDATE airlines SET flights = (SELECT id FROM flights WHERE airlineName = 'City Airline AB') WHERE name = 'City Airline AB'
UPDATE airlines SET flights = (SELECT id FROM flights WHERE airlineName = 'Scandinavian Airlines') WHERE name = 'Scandinavian Airlines'
UPDATE airlines SET flights = (SELECT id FROM flights WHERE airlineName = 'Nordic Regional') WHERE name = 'Nordic Regional'