import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;


public final class DatabaseHelper {
	private DatabaseHelper() {}
	
	private static Connection conn = null;
	
	private static String USER 			= DatabaseCredentials.getUser();
	private static String PASSWORD 		= DatabaseCredentials.getPassword();
	private static String JDBC_DRIVER 	= DatabaseCredentials.getDriver();
	private static String DB_URL 		= DatabaseCredentials.getURL();
	
	private static void openConnection() {
		try {
			Class.forName(JDBC_DRIVER);
			DatabaseHelper.conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);		
		} catch (ClassNotFoundException e) {
			System.out.println("Could not find the MySQL drivers.");
			// e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Could not connect to the database. Is it running?");
			// e.printStackTrace();
		}		
	}
	
	private static void closeConnection() {
		try {
			DatabaseHelper.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Boolean areTablesCreated() {
		DatabaseHelper.openConnection();
		
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.execute("SHOW tables");
			ResultSet rs = stmt.getResultSet();
			
			if (!rs.isBeforeFirst()) {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		DatabaseHelper.closeConnection();
		return true;
	}
	
	public static Boolean isExist(String table, String column, String value) {
		DatabaseHelper.openConnection();
		
		if (column.equals("deptDate")) {
			
			String shortDate = value.substring(0, 11);			
			try {
				Statement stmt = conn.createStatement();
				stmt.executeQuery("SELECT * FROM flights WHERE deptDate LIKE '" + shortDate + "%'");
				ResultSet rs = stmt.getResultSet();
				
				if (rs.isBeforeFirst()) { return true; }
			} catch (SQLException e) { e.printStackTrace(); }
			
		} else {
			
			try {
				Statement stmt = conn.createStatement();
				stmt.executeQuery("SELECT * FROM " + table + " WHERE " + column + " = '" + value + "'");
				ResultSet rs = stmt.getResultSet();
				
				// If the rs contains anything
				if (rs.isBeforeFirst()) { return true; }
			} catch (SQLException e) { e.printStackTrace(); }	
			
		}
		

		DatabaseHelper.closeConnection();
		return false;
	}
	
	public static int insertData(String table, HashMap<String, String> dataMap) {
		
		switch (table) {
		case "airline":
			return DatabaseHelper.insertAirline(dataMap);

		case "airport":
			return DatabaseHelper.insertAirport(dataMap);
			
		case "flight":
			return DatabaseHelper.insertFlight(dataMap);
			
		case "ticket":
			return DatabaseHelper.insertTicket(dataMap);
			
		default:
			System.out.println("ERROR: Invalid tablename.");
			break;
		}
		
		return 0;
	}
	
	private static int insertAirline(HashMap<String, String> dataMap) {
		DatabaseHelper.openConnection();
		
		String airlineName = dataMap.get("name");
 		String query = "INSERT INTO airlines(name) VALUES('" + airlineName + "')";  
		int uId = 0;
		
		uId = insertQuery(query);
		
		DatabaseHelper.closeConnection();
		return uId;
	}
	
	private static int insertAirport(HashMap<String, String> dataMap) {
		DatabaseHelper.openConnection();
		
		String airportName = dataMap.get("name");
		String airportCode = dataMap.get("code");
		
		String query = "INSERT INTO airports(name, code) VALUES ('" + airportName + "', '" + airportCode + "')"; 
		
		int uId = 0;
		
		uId = insertQuery(query);
		
		DatabaseHelper.closeConnection();
		return uId;
	}
	
	private static int insertFlight(HashMap<String, String> dataMap) {
		DatabaseHelper.openConnection();
		
		int uId = 0;
		String airlineName 		= dataMap.get("airlineName");
		String deptAirport 		= dataMap.get("deptAirport");
		String arrAirport 		= dataMap.get("arrAirport");
		String deptDate 		= dataMap.get("deptDate");
		String flightNumber 	= dataMap.get("flightNumber");
				
		StringBuilder sb = new StringBuilder();
		
		sb.append("INSERT INTO flights (flightNumber, airlineName, deptDate, deptAirport, arrAirport)");
		sb.append(" ");
		sb.append("VALUES (" + flightNumber + ", '" + airlineName + "', '" + deptDate + "', '" + deptAirport + "', '" + arrAirport + "')");
		String query = sb.toString();
		sb = null;
		
		uId = insertQuery(query);
		
		query = ("SELECT id FROM airlines WHERE name = '" + airlineName + "'");
		int airlineId = Integer.parseInt((selectQuery(query)));
		
		query = ("SELECT flights FROM airlines WHERE id = " + airlineId);
		String airlineFlights = selectQuery(query);
		if (airlineFlights == null) { airlineFlights = ""; }
		
		sb = new StringBuilder();
		sb.append(airlineFlights);
		
		if (airlineFlights.isEmpty()) {
			sb.append(uId);
		} else {
			sb.append(":" + uId);
		}
		
		String newAirlineFlights = sb.toString();
		
		query = ("UPDATE airlines SET flights = '" + newAirlineFlights + "' WHERE id = " + airlineId);
		updateQuery(query);
		
		DatabaseHelper.closeConnection();
		return uId;
	}

	private static int insertTicket(HashMap<String, String> dataMap) {
		DatabaseHelper.openConnection();
		int uId = 0;

		String flightId = dataMap.get("flightId");
		String fName = dataMap.get("fname");
		String lName = dataMap.get("lname");
		
		String query = "INSERT INTO tickets(fname, lname, flight)"
				+ "VALUES ('" + fName + "', '" + lName + "', " + flightId + ")";
		
		uId = DatabaseHelper.insertQuery(query);
		
		DatabaseHelper.closeConnection();
		return uId;
	}
	
	
	private static int insertQuery(String query) {
		int uId = 0;
	
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next(); uId = rs.getInt(1);
		} catch (SQLException e) { e.printStackTrace(); }
		
		return uId;		
	}
	
	private static void updateQuery(String query) {
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static String selectQuery(String query) {
		String retVal = "";
		
		try {
			Statement stmt = conn.createStatement();
			stmt.execute(query);
			ResultSet rs = stmt.getResultSet();
			rs.next(); retVal = rs.getString(1);			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return retVal;
	}
	
	
	public static HashMap<Integer, Airline> loadAirlineMap() {
		DatabaseHelper.openConnection();
		HashMap<Integer, Airline> airlineMap = new HashMap<Integer, Airline>(); 
		
		try {
			
			Statement stmt = conn.createStatement();
			stmt.execute("SELECT * FROM airlines");
			ResultSet rs = stmt.getResultSet();
			
			while (rs.next()) {
				int uId = rs.getInt(1);
				String airlineName = rs.getString(2);
				String flightList = rs.getString(3);
				
				Airline newAirline = new Airline(airlineName);
				newAirline.setId(uId);
				
				if (flightList != null) {
					String[] flights = flightList.split(":");
					for (int i = 0; i < flights.length; i++) {
						newAirline.addFlight(Integer.parseInt(flights[i]));
					}
				}
				
				airlineMap.put(uId, newAirline);
			}
			
		} catch (SQLException e) { e.printStackTrace(); }		
		DatabaseHelper.closeConnection();		
		return airlineMap;
	}
	
	public static HashMap<Integer, Flight> loadFlightMap() {
		DatabaseHelper.openConnection();
		HashMap<Integer, Flight> flightMap = new HashMap<Integer, Flight>();
		
		try {
			Statement stmt = conn.createStatement();
			stmt.execute("SELECT * FROM flights");
			ResultSet rs = stmt.getResultSet();
			
			while (rs.next()) {
				int uId = rs.getInt(1);
				int flightNumber = rs.getInt(2);
				String airlineName = rs.getString(3);
				String deptDate = rs.getString(4);
				String deptAirport = rs.getString(5);
				String arrAirport = rs.getString(6);
				
				Flight newFlight = new Flight(Integer.toString(flightNumber), airlineName, deptDate, deptAirport, arrAirport);
				newFlight.setId(uId);
				
				flightMap.put(uId, newFlight);
			}
		} catch (SQLException e) { e.printStackTrace(); }
		
		
		DatabaseHelper.closeConnection();
		return flightMap;
	}

	public static HashMap<Integer, Airport> loadAirportMap() {
		DatabaseHelper.openConnection();
		HashMap<Integer, Airport> airportMap = new HashMap<Integer, Airport>();
		
		try {
			Statement stmt = conn.createStatement();
			stmt.execute("SELECT * FROM airports");
			ResultSet rs = stmt.getResultSet();
			
			while (rs.next()) {
				int uId  = rs.getInt(1);
				String airportName = rs.getString(2);
				String airportCode = rs.getString(3);
				
				Airport newAirport = new Airport(airportName, airportCode);
				newAirport.setId(uId);
				
				airportMap.put(uId, newAirport);
			}
		} catch (SQLException e) { e.printStackTrace(); }
		
		
		DatabaseHelper.closeConnection();
		return airportMap;
	}
	
	public static HashMap<Integer, Ticket> loadTicketMap() {
		DatabaseHelper.openConnection();
		HashMap<Integer, Ticket> ticketMap = new HashMap<Integer, Ticket>();
	
		try {
			Statement stmt = conn.createStatement();
			stmt.execute("SELECT * FROM tickets");
			ResultSet rs = stmt.getResultSet();
			
			while (rs.next()) {
				int uId = rs.getInt(1);
				String fName = rs.getString(2);
				String lName = rs.getString(3);
				int flightId = rs.getInt(4);
				
				Ticket newTicket = new Ticket(flightId, fName, lName);
				newTicket.setId(uId);
				
				ticketMap.put(uId, newTicket);
			}
		} catch (SQLException e) { e.printStackTrace(); }
		
		DatabaseHelper.closeConnection();
		return ticketMap;
	}
	
}
