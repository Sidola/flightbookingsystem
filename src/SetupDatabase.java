import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class SetupDatabase {
	private SetupDatabase() {}
	
	private static final String USER 		= DatabaseCredentials.getUser();
	private static final String PASSWORD 	= DatabaseCredentials.getPassword();
	private static final String JDBC_DRIVER = DatabaseCredentials.getDriver(); 
	private static final String DB_URL 		= DatabaseCredentials.getURL();
	
	public static void main(String[] args) {
		
		Connection conn = null;
		Statement stmt = null;
		FileInputStream fileInput = null;
		BufferedReader fileReader = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);

			stmt = conn.createStatement();
			stmt.execute("SHOW tables");
			ResultSet rs = stmt.getResultSet();
			
			// Use this to detect if there are any tables in this database already
			// If there are, abort the process and ask the user to clear the database first
			
			if (rs.isBeforeFirst() ) {
				System.out.println("This database already has tables.\nRemove all tables and try again.");
				return;
			}
			
			// Create all neccesary tables and dummy data here
			fileInput = new FileInputStream("data/database.sql");
			fileReader = new BufferedReader(new InputStreamReader(fileInput));

            String line = fileReader.readLine();
            while(line != null){
            	stmt = conn.createStatement();
            	stmt.execute(line);
                line = fileReader.readLine();
            } 
			
            fileReader.close();
            System.out.println("Database created succesfully!");
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}