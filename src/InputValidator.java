import java.util.Scanner;


public final class InputValidator {
	private InputValidator() {}
	
	private static String validErrorReason;
	private static final Scanner inputScanner = new Scanner(System.in);
	private static Boolean stopInput = false; 
	
	private static void setErrorReason(String newReason) { validErrorReason = newReason; }
	private static String getErrorReason() { return validErrorReason; }
	
	public static Object getInput(String inputLabel, String textType) {
		Object inputData = null;
		Boolean invalidInput = true;
		stopInput = false;
		
		System.out.print(inputLabel);		
		while (invalidInput) {
			inputData = inputScanner.nextLine();
			
			// I don't even
			if (inputData.toString().toLowerCase().equals("cancel")) {
				stopInput = true;
				return null;
			}
			
			if (validateText(textType, inputData)) {
				invalidInput = false;
			} else {
				System.out.println(getErrorReason());
				System.out.print(inputLabel);
			}
		}
		return inputData;
	}
		
	public static Boolean validateText(String textType, Object inputData) {
		setErrorReason(null); // Reset error message for each call
		
		switch (textType) {
		
		case "airlineName":
			return validateAirlineName(inputData);

		case "airportCode":
			return validateAirportCode(inputData);
		
		case "date":
			return validateDate(inputData);
			
		case "flightNumber":
			return validateFlightNumber(inputData);
			
		case "airportName":
			return validateAirportName(inputData);
			
		default:
			setErrorReason("Invalid validation type.");
			return false;
		}
	}
	
	// Specific validators
	
	private static Boolean validateAirlineName(Object inputData) {
		String airlineName = (String) inputData;
		
		// Length check
		if (airlineName.length() < 6) { 
			setErrorReason("Airline name too short - it must be at least 6 symbols long");
			return false; 
		}
				
		return true;
	}
	
	private static Boolean validateAirportCode(Object inputData) {
		String airportCode = (String) inputData;
		
		// Lenght check
		if ((airportCode.length() < 3)
		|| (airportCode.length() > 4)) {
			setErrorReason("Airport code wrong length - it must be exactly 3 letters");
			return false;
		}
		
		// Type check
		if (!isAlpha(airportCode)) {
			setErrorReason("Airport code contains invalid symbols - only use letters");
			return false;
		}
		
		return true;
	}
	
	private static Boolean validateDate(Object inputData) {
		String date = (String) inputData;
		
		// Format check
		if (!date.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}\\s[0-9]{2}:[0-9]{2}:[0-9]{2}")) {
			setErrorReason("Invalid date format - Expected YYYY-MM-DD HH:MM:SS");
			return false;
		}
		
		return true;
	}
	
	private static Boolean validateFlightNumber(Object inputData) {
		return true;
	}
	
	private static Boolean validateAirportName(Object inputData) {
		
		// Check unique name
		
		return true;
	}
	
	// Helpers
	
	public static Boolean stopInput() {
		if (stopInput) {
			System.out.println(TextConstants.SEPARATOR_LINE);
			System.out.println("Stopped input for the current command.");
			System.out.println(TextConstants.SEPARATOR_LINE);			
		}
		return stopInput;
	}
	
	public static Boolean isAlpha(String name) {
	    char[] chars = name.toCharArray();

	    for (char c : chars) {
	        if(!Character.isLetter(c)) {
	            return false;
	        }
	    }

	    return true;
	}
}
