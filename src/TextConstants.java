
public final class TextConstants {
	private TextConstants() {};
	
	public static final String SEPARATOR_LINE = "----------------";
	public static final String SEPARATOR_LINE_DOUBLE = "--------------------------------";
	public static final String WELCOME_MESSAGE = "Welcome, enter a command to begin.";
	public static final String CANCEL_TYPE_INSTRUCTIONS = "Type CANCEL at any time to cancel the current operation.\nType HELP to see the commands again.";
	public static final String AVAIL_COMMANDS = "ADD [airport|airline|flight]\nLIST [airport|airline|flight|ticket]\nFIND [flight|ticket]\nBOOK [flight]";
	
	public static final String COMMANDS_LEGEND = SEPARATOR_LINE + "\n" 
											   + AVAIL_COMMANDS + "\n"
											   + SEPARATOR_LINE + "\n"
											   + CANCEL_TYPE_INSTRUCTIONS + "\n"
											   + SEPARATOR_LINE;	
	
}
