
public final class DatabaseCredentials {
	private DatabaseCredentials() {}
	
	// CONFIGURE YOUR SETTINGS HERE
	private static final String USER = "root";
	private static final String PASSWORD = "rXt1el99DV6SStM";
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	private static final String DB_URL = "jdbc:mysql://localhost/book_flights";
	
	// Getters
	public static String getUser() { return USER; }
	public static String getPassword() { return PASSWORD; }
	public static String getDriver() { return JDBC_DRIVER; }
	public static String getURL() { return DB_URL; };
}
