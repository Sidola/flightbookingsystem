
public class Flight extends DataObject {

	// Instance variables
	
	private int flightNumber;
	private String airlineName;
	private String deptDate;
	private String deptAirport;
	private String arrAirport;
	
	// Constructor
	
	public Flight(String flightNumber, String airlineName, String deptDate, String deptAirport, String arrAirport) {
		
		this.flightNumber = Integer.parseInt(flightNumber);
		this.airlineName = airlineName;
		this.deptDate = deptDate;
		this.deptAirport = deptAirport;
		this.arrAirport = arrAirport;
		
	}
	
	// Getters
	
	public int getFlightNumber() 	{ return flightNumber; }
	public String getDeptDate() 	{ return deptDate; }
	public String getAirlineName() 	{ return airlineName; }
	public String getDeptAirport() 	{ return deptAirport; }
	public String getArrAirport() 	{ return arrAirport; }
	
}
