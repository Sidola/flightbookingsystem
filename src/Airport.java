
public class Airport extends DataObject {
	
	// Instance variables
	
	private String airportName;
	private String airportCode;
	
	// Constructor
	
	public Airport(String name, String code) {
		airportName = name;
		airportCode = code;
	}
	
	// Getters
	
	public String getName() { return airportName; }
	public String getCode() { return airportCode; }
	
}
