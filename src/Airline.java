import java.util.ArrayList;

public class Airline extends DataObject {

	// Instance variables
	
	private String airlineName;
	private ArrayList<Integer> listOfFlights = new ArrayList<Integer>();

	// Constructor
	
	public Airline(String airlineName) {
		this.airlineName = airlineName;
	}

	// Getters
	
	public String getName() { return airlineName; }
	public ArrayList<Integer> getFlights() { return listOfFlights; }
	
	
	// Functional methods	
	
	/**
	 * Add a flight to the airline.
	 * 
	 * @param flightId - the ID of the flight to add
	 * @return returns true if the flight was added successfully
	 */
	public Boolean addFlight(int flightId) {		
	
		for (Integer integer : listOfFlights) {
			if (flightId == integer) {
				System.out.println("This flight already exists for this airline.");
				return false;
			}
		}
		
		listOfFlights.add(flightId);
		return true;
	}
}
