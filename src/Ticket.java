
public class Ticket extends DataObject {

	// Instance variables
	private int flightId;
	private String fName;
	private String lName;
	
	// Constructor
	public Ticket(int flightId, String fName, String lName) {
		this.flightId = flightId;
		this.fName = fName;
		this.lName = lName;
	}
	
	// Getters
	public int getFlightId() { return flightId; }
	public String getFname() { return fName; }
	public String getLname() { return lName; }
}
