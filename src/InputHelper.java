import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public final class InputHelper {
	private InputHelper() {}

	// Instance variables
	
	private static Scanner inputScanner = new Scanner(System.in);
	private static Boolean acceptInput = true;
	
	// Init & helper methods
	
	public static void init() { InputHelper.inputConsole(); }
	private static void println(String text) { System.out.println(text); }
	private static void print(String text) { System.out.print(text); }
	private static void wrapInLines(String text) {
		println(TextConstants.SEPARATOR_LINE);
		println(text);
		println(TextConstants.SEPARATOR_LINE);
	}
		
	private static void inputConsole() {
		
		println(TextConstants.WELCOME_MESSAGE);
		println(TextConstants.COMMANDS_LEGEND);
		
		while (acceptInput) {
			print("> ");
			String input = inputScanner.nextLine();
			
			String[] inputParts = input.split("\\s");
			inputParts[0] = inputParts[0].toLowerCase();
						
			if ((inputParts.length < 2) && !inputParts[0].equals("help")) {
				wrapInLines("Input too short.");
				continue;
			}
			
			switch (inputParts[0]) {
			case "add":
				InputHelper.addData(inputParts[1]);
				break;
				
			case "list":
				InputHelper.listData(inputParts[1]);
				break;
			
			case "find":
				InputHelper.findData(inputParts[1]);
				break;
				
			case "book":
				InputHelper.bookFlight();
				break;
				
			case "help":
				println(TextConstants.COMMANDS_LEGEND);
				break;
				
			default:
				wrapInLines("Invalid command: " + inputParts[0]);
				break;
			}
		}
	}
	
	// Command methods ------------------------------
	
	/**
	 * Adds data to the program
	 * 
	 * @param dataType - Type of data to add
	 */
	private static void addData(String dataType) {
		dataType = dataType.toLowerCase();
		
		switch (dataType) {
		case "airline":
			InputHelper.addAirline();
			break;

		case "airport":
			InputHelper.addAirport();
			break;
		
		case "flight":
			InputHelper.addFlight();
			break;
			
		default:
			wrapInLines("Unkown command: " + dataType);
			break;
		}		
	}

	private static void addFlight() {
		HashMap<String, String> flightData = new HashMap<String, String>();
		String flightNumber, airlineName, deptDate, deptAirport, arrAirport;
		
		println(TextConstants.SEPARATOR_LINE);
		println("Specify the following data to add a flight:");
		println("- Airline name");
		println("- Departure airport (3 letter code)");
		println("- Arrival airport (3 letter code)");
		println("- Departure date (YYYY-MM-DD HH:MM:SS)");
		println("- Flight number");
		println(TextConstants.SEPARATOR_LINE);
		
		airlineName = (String) InputValidator.getInput("Airline name: ", "airlineName");		
		if (InputValidator.stopInput()) { return; } 
		
		deptAirport = (String) InputValidator.getInput("Departure airport: ", "airportCode");
		if (InputValidator.stopInput()) { return; } 
		
		arrAirport = (String) InputValidator.getInput("Arrival airport: ", "airportCode");		
		if (InputValidator.stopInput()) { return; } 
		
		deptDate = (String) InputValidator.getInput("Departure date: ", "date");
		if (InputValidator.stopInput()) { return; } 
		
		flightNumber = (String) InputValidator.getInput("Flight number: ", "flightNumber");
		if (InputValidator.stopInput()) { return; } 
		
		flightData.put("airlineName", airlineName);
		flightData.put("deptAirport", deptAirport);
		flightData.put("arrAirport", arrAirport);
		flightData.put("deptDate", deptDate);
		flightData.put("flightNumber", flightNumber);
		
		DataHelper.saveData("flight", flightData);
	}

	private static void addAirline() {
		
		HashMap<String, String> airlineData = new HashMap<String, String>(); 
		String airlineName;
		
		println(TextConstants.SEPARATOR_LINE);
		println("To add an airline, specify the name of the airline.");
		println(TextConstants.SEPARATOR_LINE);
				
		airlineName = (String) InputValidator.getInput("Airline name: ", "airlineName");		
		if (InputValidator.stopInput()) { return; } 
		
		airlineData.put("name", airlineName);
		
		DataHelper.saveData("airline", airlineData);
	}

	private static void addAirport() {
		
		HashMap<String, String> airportData = new HashMap<String, String>();
		String airportName;
		String airportCode;
		
		println(TextConstants.SEPARATOR_LINE);
		println("To add an airport, specify the following data:");
		println("- Name of the airport");
		println("- Airport code (3 letters)");
		println(TextConstants.SEPARATOR_LINE);
				
		airportName = (String) InputValidator.getInput("Airport name: ", "airportName");
		if (InputValidator.stopInput()) { return; } 
		
		airportCode = (String) InputValidator.getInput("Airport code: ", "airportCode");
		if (InputValidator.stopInput()) { return; } 
		
		airportData.put("name", airportName);
		airportData.put("code", airportCode);
		
		DataHelper.saveData("airport", airportData);		
	}
	
	// New list methods
	private static void listData(String dataType) {
		dataType = dataType.toLowerCase();
		
		switch (dataType) {
		case "airline":
			InputHelper.listAirlines();
			break;

		case "airport":
			InputHelper.listAirports();
			break;
		
		case "flight":
			InputHelper.listFlights();
			break;
		
		case "ticket":
			InputHelper.listTickets();
			break;
			
		default:
			System.out.println("Unkown command: " + dataType);
			break;
		}	
	}
	
	private static void listAirlines() {
		
		HashMap<Integer, Airline> airlineMap = DataHelper.getAirlineMap();
		HashMap<Integer, Flight> flightMap = DataHelper.getFlightMap();		
		
		println(TextConstants.SEPARATOR_LINE);
		for (Map.Entry<Integer, Airline> entry : airlineMap.entrySet()) {
		    Integer key = entry.getKey();
		    Airline airline = entry.getValue();
		    
		    println("Name: " + airline.getName());
		    println("Flights: ");
		    if (!airline.getFlights().isEmpty()) {
	    	
		    	for (Integer flightId : airline.getFlights()) {
		    		String airPorts = flightMap.get(flightId).getDeptAirport() + " - " + flightMap.get(flightId).getArrAirport();
		    		String deptDate = flightMap.get(flightId).getDeptDate();
		    		println(airPorts + " // " + deptDate);
				}
		    	
		    }
		    println(TextConstants.SEPARATOR_LINE);
		}	
	}
	
	private static void listAirports() {
		HashMap<Integer, Airport> airportMap = DataHelper.getAirportMap();
		
		println(TextConstants.SEPARATOR_LINE);
		for (Map.Entry<Integer, Airport> entry : airportMap.entrySet()) {
			Integer key = entry.getKey();
			Airport airport = entry.getValue();
			
			println(airport.getCode() + " - " + airport.getName());
			println(TextConstants.SEPARATOR_LINE);			
		}
	}

	private static void listFlights() {
		HashMap<Integer, Flight> flightMap = DataHelper.getFlightMap();
		
		println(TextConstants.SEPARATOR_LINE);
		for (Map.Entry<Integer, Flight> entry : flightMap.entrySet()) {
			Integer key = entry.getKey();
			Flight flight = entry.getValue();
			
			println("#" + flight.getId() + "\t" + flight.getDeptAirport() + " - " + flight.getArrAirport());
			println("\tDeparture date: " + flight.getDeptDate());
			println("\tAirline: " + flight.getAirlineName());
			println("\tFlight-number: " + flight.getFlightNumber());
			println(TextConstants.SEPARATOR_LINE);
		}
		
		println("To book a flight, enter the ID of the flight, which can be seen on the left.");
		println("To go back, enter cancel or blank.");
		println(TextConstants.SEPARATOR_LINE);
		print("> ");
		
		String flightId = inputScanner.nextLine(); // validate
		
		if ((flightId.equals("cancel")) 
	    || flightId.equals("blank") 
	    || flightId.isEmpty()) { 
			println(TextConstants.SEPARATOR_LINE);
			println("Booking canceled.");
			println("Enter a command to continue.");
			println(TextConstants.SEPARATOR_LINE);
			println(TextConstants.AVAIL_COMMANDS);
			println(TextConstants.SEPARATOR_LINE);
			return;
		}
		
		int intFlightId = Integer.parseInt(flightId);
		bookTicket(intFlightId);
		
	}

	private static void listTickets() {
		HashMap<Integer, Ticket> ticketMap = DataHelper.getTicketMap();
		HashMap<Integer, Flight> flightMap = DataHelper.getFlightMap();
		
		println(TextConstants.SEPARATOR_LINE);
		println("Listing flights that people have booked tickets for.");	
		println(TextConstants.SEPARATOR_LINE);
				
		for(Map.Entry<Integer, Flight> entry : flightMap.entrySet()) {
			int key = entry.getKey();
			Flight flight = entry.getValue();
			
			StringBuilder sb = new StringBuilder();
			
			for(Map.Entry<Integer, Ticket> subEntry : ticketMap.entrySet()) {
				int subKey = subEntry.getKey();
				Ticket ticket = subEntry.getValue();
				
				if (ticket.getFlightId() == key) {
					sb.append("\t" + ticket.getFname() + " " + ticket.getLname() + "\n");
				}
			}
			
			String passengers = sb.toString().replaceFirst("\\n$", "");
			if (!passengers.isEmpty()) {
				println("#" + flight.getId() + "\t" + flight.getDeptAirport() + " - " + flight.getArrAirport());
				println("\tDeparture date: " + flight.getDeptDate());
				println("\tAirline: " + flight.getAirlineName());
				println("\tFlight-number: " + flight.getFlightNumber());
				println("");
				println("\tPassangers:");
				println("\t----------");
				println(passengers);				
				println(TextConstants.SEPARATOR_LINE);
			}
		}
 		
		
		
	}
	
	
	private static void findData(String dataType) {
		dataType = dataType.toLowerCase();
		
		switch (dataType) {
		case "flight":
			InputHelper.findFlights();
			break;

		case "ticket":
			InputHelper.findTicket();
			break;
			
		default:
			println("Unknown command: " + dataType);
			break;
		}
	}
	
	private static void findFlights() {
		
		println(TextConstants.SEPARATOR_LINE);
		println("To find flights, specify the following data:");
		println("- Departure airport (3 letter code)");
		println("- Arrival airport (3 letter code)");
		println("- (Optional) Departure date (YYYY-MM-DD)");
		println(TextConstants.SEPARATOR_LINE);
		
		String deptAirport, arrAirport, deptDate;
		
		print("Departure airport: ");
		deptAirport = inputScanner.nextLine(); // Vaaaalidate
		
		print("Arrival airport: ");
		arrAirport = inputScanner.nextLine(); // Vaaaalidate
		
		print("Departure date: ");
		deptDate = inputScanner.nextLine(); // Vaaaalidate
		
		ArrayList<Flight> matchingFlightsList = new ArrayList<Flight>();
		HashMap<Integer, Flight> flightMap = DataHelper.getFlightMap();
		
		for (Map.Entry<Integer, Flight> entry : flightMap.entrySet()) {
			
			int key = entry.getKey();
			Flight flight = entry.getValue();
			
			String dAp = flight.getDeptAirport();
			String aAp = flight.getArrAirport();
			String dDate = flight.getDeptDate();
			
			if (dAp.equals(deptAirport)
		    && (aAp.equals(arrAirport))) {
				
				// If the user entered a date
				if (!deptDate.isEmpty()) {
					String flightDeptDate = dDate;
					if (flightDeptDate.equals(deptDate)) { matchingFlightsList.add(flight); }
				} else {
					matchingFlightsList.add(flight);
				}
			}
		}
		
		println(TextConstants.SEPARATOR_LINE);
		println("Matching flights:");
		println(TextConstants.SEPARATOR_LINE);
		for (Flight flight : matchingFlightsList) {
			println("#" + flight.getId() + "\t" + flight.getDeptAirport() + " - " + flight.getArrAirport());
			println("\tDeparture date: " + flight.getDeptDate());
			println("\tAirline: " + flight.getAirlineName());
			println("\tFlight-number: " + flight.getFlightNumber());
			println(TextConstants.SEPARATOR_LINE);			
		}
		
		println("To book a flight, enter the ID of the flight, which can be seen on the left.");
		println("To go back, enter cancel or blank.");
		println(TextConstants.SEPARATOR_LINE);
		print("> ");
		
		String flightId = inputScanner.nextLine(); // validate
		
		if ((flightId.equals("cancel")) 
	    || flightId.equals("blank") 
	    || flightId.isEmpty()) { 
			println(TextConstants.SEPARATOR_LINE);
			println("Booking canceled.");
			println("Enter a command to continue.");
			println(TextConstants.SEPARATOR_LINE);
			println(TextConstants.AVAIL_COMMANDS);
			println(TextConstants.SEPARATOR_LINE);
			return;
		}
		
		int intFlightId = Integer.parseInt(flightId);
		bookTicket(intFlightId);
		
	}
	
	private static void findTicket() {
		println(TextConstants.SEPARATOR_LINE);
		println("To find a ticket, specify the following information:");
		println("- First name");
		println("- Last name");
		println(TextConstants.SEPARATOR_LINE);
		
		print("First name: ");
		String fName = inputScanner.nextLine();
		
		print("Last name: ");
		String lName = inputScanner.nextLine();
		
		println(TextConstants.SEPARATOR_LINE);
		
		HashMap<Integer, Flight> flightMap = DataHelper.getFlightMap();
		HashMap<Integer, Ticket> ticketMap = DataHelper.getTicketMap();
		
		for (Map.Entry<Integer, Ticket> entry : ticketMap.entrySet()) {
			int key = entry.getKey();
			Ticket ticket = entry.getValue();
			
			String thisFname = ticket.getFname();
			String thisLname = ticket.getLname();
			int thisFlight = ticket.getFlightId();
			
			if ((thisFname.equals(fName)) 
		    && (thisLname.equals(lName))) {
				for (Map.Entry<Integer, Flight> subEntry : flightMap.entrySet()) {
					int subKey = subEntry.getKey();
					Flight flight = subEntry.getValue();
					
					if (flight.getId() == thisFlight) {
					
						// THIS IS A MATCHING FLIGHT
						println("#" + flight.getId() + "\t" + flight.getDeptAirport() + " - " + flight.getArrAirport());
						println("\tDeparture date: " + flight.getDeptDate());
						println("\tAirline: " + flight.getAirlineName());
						println("\tFlight-number: " + flight.getFlightNumber());
						println(TextConstants.SEPARATOR_LINE);	
					}	
				}
			}
		}
		
		
	}
	
	
	private static void bookFlight() {
		listFlights();
	}
	
	private static void bookTicket(int flightId) {
		
		println(TextConstants.SEPARATOR_LINE);
		println("You're booking flight - INFO HERE -");
		println("Please specify the following to complete the booking: ");
		println("- First name");
		println("- Last name");
		println(TextConstants.SEPARATOR_LINE);
		
		HashMap<String, String> ticketData = new HashMap<String, String>();
		String fName, lName;
		
		print("First name: "); // Validate?
		fName = inputScanner.nextLine();
		print("Last name: "); // Validate?
		lName = inputScanner.nextLine();

		ticketData.put("flightId", Integer.toString(flightId));
		ticketData.put("fname", fName);
		ticketData.put("lname", lName);
		
		DataHelper.saveData("ticket", ticketData);
	}
	
}




























