import java.util.HashMap;
import java.util.Map;

public final class DataHelper {
	private DataHelper() {}
		
	private static HashMap<Integer, Airline> airlineMap = new HashMap<Integer, Airline>();
	private static HashMap<Integer, Flight> flightMap = new HashMap<Integer, Flight>();
	private static HashMap<Integer, Airport> airportMap = new HashMap<Integer, Airport>();
	private static HashMap<Integer, Ticket> ticketMap = new HashMap<Integer, Ticket>();
	
	public static Boolean loadData() {
		
		// Check if database exists and works
		if (!DatabaseHelper.areTablesCreated()) {
			System.out.println("Could not find a working database. Run SetupDatabase.java first.");
			return false;
		}
		
		airlineMap = DatabaseHelper.loadAirlineMap();
		flightMap = DatabaseHelper.loadFlightMap();
		airportMap = DatabaseHelper.loadAirportMap();
		ticketMap = DatabaseHelper.loadTicketMap();
		return true;
	}
	
	public static void saveData(String dataType, HashMap<String, String> dataMap) {
	
		switch (dataType) {
		case "airline":
			DataHelper.saveAirline(dataMap);
			break;

		case "airport":
			DataHelper.saveAirport(dataMap);
			break;
		
		case "flight":
			DataHelper.saveFlight(dataMap);
			break;
			
		case "ticket":
			DataHelper.saveTicket(dataMap);
			break;
			
		default:
			break;
		}
	}
	
	private static Boolean saveAirline(HashMap<String, String> dataMap) {
		
		// Check if this airline already exists in the database
		if (DatabaseHelper.isExist("airlines", "name", dataMap.get("name"))) {
			System.out.println(TextConstants.SEPARATOR_LINE);
			System.out.println("That airline already exists. Your data will not be saved.");
			System.out.println(TextConstants.SEPARATOR_LINE);
			return false;
		}
		
		// Create object from the data we have
		Airline newAirline = new Airline(dataMap.get(("name")));
		
		// Add data to database for an index
		int uId = DatabaseHelper.insertData("airline", dataMap);
		newAirline.setId(uId);
		
		System.out.println(TextConstants.SEPARATOR_LINE);
		System.out.println("Successfully added airline: " + newAirline.getName());
		System.out.println(TextConstants.SEPARATOR_LINE);
		
		airlineMap.put(uId, newAirline);
		
		// Finished
		return true;
	}
	
	private static Boolean saveAirport(HashMap<String, String> dataMap) {
		
		// if name or code already exists
		if ( (DatabaseHelper.isExist("airports", "name", dataMap.get("name"))) || (DatabaseHelper.isExist("airports", "code", dataMap.get("code"))) ) {
			System.out.println(TextConstants.SEPARATOR_LINE);
			System.out.println("This airport already exists. Your data will not be saved.");
			System.out.println(TextConstants.SEPARATOR_LINE);
			return false;
		}
		
		Airport newAirport = new Airport(dataMap.get("name"), dataMap.get("code"));
		
		int uId = DatabaseHelper.insertData("airport", dataMap);
		newAirport.setId(uId);
		
		airportMap.put(uId, newAirport);
		
		System.out.println(TextConstants.SEPARATOR_LINE);
		System.out.println("Successfully added airport: " + newAirport.getName());
		System.out.println(TextConstants.SEPARATOR_LINE);
		return true;
	}
	
	private static Boolean saveFlight(HashMap<String, String> dataMap) {
		
		String airlineName = dataMap.get("airlineName");
		String deptAirport = dataMap.get("deptAirport");
		String arrAirport = dataMap.get("arrAirport");
		String deptDate = dataMap.get("deptDate");
		String flightNumber = dataMap.get("flightNumber");
		
		// This need to check date, not data and time
		if (DatabaseHelper.isExist("flights", "deptDate", deptDate)) {
			System.out.println(TextConstants.SEPARATOR_LINE);
			System.out.println("There's already a flight for this date. Try another date.");
			System.out.println(TextConstants.SEPARATOR_LINE);
			return false;
		}
		
		// Make sure the airline & airports exist
		if (!DatabaseHelper.isExist("airlines", "name", airlineName)) {
			System.out.println(TextConstants.SEPARATOR_LINE);
			System.out.println("There's no airline with that name. Try another airline.");
			System.out.println(TextConstants.SEPARATOR_LINE);
			return false;
		}
		
		if (!DatabaseHelper.isExist("airports", "code", deptAirport) 
		 || !DatabaseHelper.isExist("airports", "code", arrAirport)) {
			System.out.println(TextConstants.SEPARATOR_LINE);
			System.out.println("There are no airports with that code. Try another code");
			System.out.println(TextConstants.SEPARATOR_LINE);
			return false;
		}
		
		Flight newFlight = new Flight(flightNumber, airlineName, deptDate, deptAirport, arrAirport);
		
		int uId = DatabaseHelper.insertData("flight", dataMap);
		newFlight.setId(uId);
		
		flightMap.put(uId, newFlight);
		
		// Add flight to the airline
		for (Map.Entry<Integer, Airline> entry : airlineMap.entrySet()) {
			int key = entry.getKey();
			Airline airline = entry.getValue();
						
			if (airline.getName().equals(airlineName)) {
				airline.addFlight(uId);
			}
			
		}
		
		System.out.println(TextConstants.SEPARATOR_LINE);
		System.out.println("Flight successfully added.");
		System.out.println(TextConstants.SEPARATOR_LINE);
		return true;
	}
	
	private static Boolean saveTicket(HashMap<String, String> dataMap) {
		int flightId = Integer.parseInt(dataMap.get("flightId"));
		String fName = dataMap.get("fname");
		String lName = dataMap.get("lname");
		String compString = flightId + fName + lName;
		
		// Check if a ticket has already been booked for this flight on this name
		for (Map.Entry<Integer, Ticket> entry : ticketMap.entrySet()) {
			Ticket ticket = entry.getValue();
			String thisCompString = ticket.getFlightId() + ticket.getFname() + ticket.getLname();
			
			if (thisCompString.equals(compString)) {
				System.out.println(TextConstants.SEPARATOR_LINE);
				System.out.println("This person already has a ticket for this flight.");
				System.out.println(TextConstants.SEPARATOR_LINE);
				return false;
			}
		}
		
		// Save the ticket in the DB
		Ticket newTicket = new Ticket(flightId, fName, lName);
		int uId = DatabaseHelper.insertData("ticket", dataMap);
		newTicket.setId(uId);
		
		ticketMap.put(uId, newTicket);
		
		// Confirm
		System.out.println(TextConstants.SEPARATOR_LINE);
		System.out.println("Your ticket has been booked!");
		System.out.println(TextConstants.SEPARATOR_LINE);
		return true;
	}

	public static HashMap<Integer, Airline> getAirlineMap() { return airlineMap; }
	public static HashMap<Integer, Flight> getFlightMap() { return flightMap; }
	public static HashMap<Integer, Airport> getAirportMap() { return airportMap; }
	public static HashMap<Integer, Ticket> getTicketMap() { return ticketMap; }
	
}
